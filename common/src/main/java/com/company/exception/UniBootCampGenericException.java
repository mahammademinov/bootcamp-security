package com.company.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Getter
@Slf4j
public class UniBootCampGenericException extends RuntimeException {

    private final int status;
    private final String code;
    private final String message;
    private final transient Object[] arguments;

    public UniBootCampGenericException(int status, String code, String message, Object[] arguments) {
        super(message);
        this.status = status;
        this.code = code;
        this.message = message;
        this.arguments = arguments == null ? new Object[0] : arguments;
    }

    public UniBootCampGenericException(String errorBody, HttpStatus statusCode) {
        super(errorBody);
        this.status = statusCode.value();
        this.code = errorBody;
        this.message = errorBody;
        this.arguments = null;
    }

    @Override
    public String toString() {
        return "UniBootCampGenericException{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}

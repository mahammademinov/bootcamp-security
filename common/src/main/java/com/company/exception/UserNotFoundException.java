package com.company.exception;

import static com.company.exception.UniBootCampErrorCodes.USER_NOT_FOUND;

public class UserNotFoundException extends UniBootCampGenericException {

    public UserNotFoundException(Object[] arguments) {
        super(404, USER_NOT_FOUND.code, USER_NOT_FOUND.code, arguments);
    }

}

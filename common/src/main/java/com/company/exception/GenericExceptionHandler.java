package com.company.exception;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

import com.company.security.TranslationRepoServiceImpl;
import java.time.OffsetDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GenericExceptionHandler {

    private final TranslationRepoServiceImpl translationRepoService;

    @ExceptionHandler(UniBootCampGenericException.class)
    public ResponseEntity<ErrorResponseDTO> handleException(UniBootCampGenericException ex, WebRequest webRequest) {
        ex.printStackTrace();
        var path = ((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        String lang= webRequest.getHeader(ACCEPT_LANGUAGE);
        return createErrorResponse(ex, path, lang);
    }

    private ResponseEntity<ErrorResponseDTO> createErrorResponse(UniBootCampGenericException ex, String path,
                                                                 String lang) {

        ErrorResponseDTO build = ErrorResponseDTO.builder()
                .status(ex.getStatus())
                .code(ex.getCode())
                .path(path)
                .timestamp(OffsetDateTime.now())
                .message(translationRepoService.findByKey(ex.getMessage(),lang,ex.getArguments()))
                .detail(translationRepoService.findByKey(ex.getMessage().concat("_DETAIL"), lang, ex.getArguments()))
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(build);
    }
}

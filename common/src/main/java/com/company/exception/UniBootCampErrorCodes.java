package com.company.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UniBootCampErrorCodes {

    USER_NOT_FOUND("USER-NOT-FOUND"),
    JWT_EXPIRED("JWT-EXPIRED");

    public final String code;
}

package com.company.security;

import java.util.Locale;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TranslationRepoServiceImpl {

    private final MessageSource messageSource;

    public String findByKey(String key, String lang, Object... arguments){
        try {
            return messageSource.getMessage(key,arguments,new Locale(lang));
        }catch (NoSuchMessageException exception){
            return "";
        }
    }
}

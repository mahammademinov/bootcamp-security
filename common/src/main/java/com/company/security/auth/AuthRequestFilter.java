package com.company.security.auth;

import static com.company.exception.UniBootCampErrorCodes.JWT_EXPIRED;

import com.company.exception.ErrorResponseDTO;
import com.company.security.auth.service.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.jsonwebtoken.ExpiredJwtException;
import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@RequiredArgsConstructor
public class AuthRequestFilter extends OncePerRequestFilter {

    private final List<AuthService> authServices;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        try {
            log.trace("Filtering request against auth services {}", authServices);
            Optional<Authentication> authOptional = Optional.empty();
            for (AuthService authService : authServices) {
                authOptional = authOptional.or(() -> authService.getAuthentication(httpServletRequest));
            }
            authOptional.ifPresent(auth -> SecurityContextHolder.getContext().setAuthentication(auth));
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (ExpiredJwtException ex) {
            ErrorResponseDTO errorResponseDTO = ErrorResponseDTO.builder()
                    .status(400)
                    .code(JWT_EXPIRED.code)
                    .path(httpServletRequest.getServletPath())
                    .timestamp(OffsetDateTime.now())
                    .message("bad request")
                    .detail("invalid credentials")
                    .build();

            OutputStream responseStream = httpServletResponse.getOutputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.writeValue(responseStream, errorResponseDTO);
            responseStream.flush();
        }
    }

}

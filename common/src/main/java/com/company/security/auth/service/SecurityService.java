package com.company.security.auth.service;

import com.company.security.constants.UserAuthority;
import java.util.Optional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    public Optional<String> getCurrentUserUsername() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> {
                    if (authentication.getPrincipal() instanceof UserDetails) {
                        UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                        return springSecurityUser.getUsername();
                    } else if (authentication.getPrincipal() instanceof String) {
                        return (String) authentication.getPrincipal();
                    }
                    return null;
                });
    }

    public Optional<UserAuthority> getCurrentUserAuthority() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return getAuthority(securityContext.getAuthentication());
    }

    public Optional<String> getCurrentUserJwt() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .filter(authentication -> authentication.getCredentials() instanceof String)
                .map(authentication -> (String) authentication.getCredentials());
    }


    public boolean isCurrentUserInRole(UserAuthority authority) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (authentication != null)
                && getAuthority(authentication).stream().anyMatch(authority::equals);
    }

    private Optional<UserAuthority> getAuthority(Authentication authentication) {
        return authentication.getAuthorities()
                .stream()
                .map(Object::toString)
                .findFirst()
                .map(UserAuthority::valueOf);
    }
}

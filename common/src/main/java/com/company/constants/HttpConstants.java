package com.company.constants;

public final class HttpConstants {
    public static final String AUTH_HEADER = "Authorization";
    public static final String BEARER_AUTH_HEADER = "Bearer";
    public static final String BASIC_AUTH_HEADER = "Basic";

    private HttpConstants() {
    }
}

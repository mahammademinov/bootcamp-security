package com.company.security;

import com.company.constants.HttpConstants;
import com.company.domain.Authority;
import com.company.domain.User;
import com.company.exception.UserNotFoundException;
import com.company.repository.UserRepository;
import com.company.security.auth.service.AuthService;
import java.util.Base64;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BasicAuthService implements AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(HttpConstants.AUTH_HEADER))
                .filter(this::isBasicAuth)
                .flatMap(this::getAuthenticationBasic);
    }

    private boolean isBasicAuth(String header) {
        return header.toLowerCase().startsWith(HttpConstants.BASIC_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBasic(String header) {
        var token = header.substring(HttpConstants.BASIC_AUTH_HEADER.length()).trim();
        byte[] decode = Base64.getDecoder().decode(token);
        String[] credentials = new String(decode).split(":");
        if (credentials.length != 2) {
            throw new RuntimeException("credentials length is not 2 ");
        }

        User user =
                userRepository.findByUsername(credentials[0]).orElseThrow(() -> new UserNotFoundException(new Object[]{credentials[0]}));
        boolean matches = passwordEncoder.matches(credentials[1], user.getPassword());
        if (matches) {
            Authentication authentication = getAuthenticationBasic(user);
            return Optional.of(authentication);
        }
        return Optional.empty();

    }

    private Authentication getAuthenticationBasic(User user) {
        var roles = user.getAuthorities().stream().map(Authority::getRole).collect(Collectors.toList());
        var authorityList = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(user, "", authorityList);
    }

}

package com.company.security;

import com.company.domain.User;
import com.company.exception.UserNotFoundException;
import com.company.repository.UserRepository;
import com.company.security.auth.service.JwtService;
import com.company.security.auth.service.SecurityService;
import com.company.service.auth.LoginRequestDto;
import java.time.Duration;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Import(SecurityService.class)
public class SecurityUtil {
//    private static final Duration ONE_WEEK = Duration.ofDays(7);

    private final UserRepository userRepository;
    private final SecurityService securityService;
    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public String createAuthentication(LoginRequestDto loginRequestDto) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return jwtService.issueToken(authentication);
    }

}

package com.company.config;

import static com.company.security.constants.UserAuthority.ADMIN;

import com.company.repository.UserRepository;
import com.company.security.BasicAuthService;
import com.company.security.auth.AuthenticationEntryPointConfigurer;
import com.company.security.auth.service.JwtService;
import com.company.security.auth.service.TokenAuthService;
import com.company.security.config.BaseSecurityConfig;
import com.company.security.config.SecurityProperties;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import({SecurityProperties.class, JwtService.class, AuthenticationEntryPointConfigurer.class})
public class SecurityConfiguration extends BaseSecurityConfig {
    private static final String AUTH_API = "/api/auth/**";
    private static final String USER_API = "/user/**";
    private static final String ADMIN_API = "/admin/**";

    public SecurityConfiguration(SecurityProperties securityProperties, JwtService jwtService,
                                 UserRepository userRepository, PasswordEncoder passwordEncoder) {
        super(securityProperties, List.of(new TokenAuthService(jwtService), new BasicAuthService(userRepository, passwordEncoder)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(AUTH_API).permitAll()
                .antMatchers(USER_API).access(authorities(ADMIN))
                .antMatchers(ADMIN_API).access(authorities(ADMIN))
                .antMatchers("/api/v1/**").authenticated();
        super.configure(http);
    }

}

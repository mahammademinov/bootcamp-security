package com.company.domain;

public enum Role {

    MANAGER,
    USER
}

package com.company.service;

import com.company.service.auth.LoginRequestDto;
import com.company.service.auth.LoginResponse;
import com.company.service.auth.RegisterRequestDto;

public interface AuthService {

    void register(RegisterRequestDto registerRequestDto);

    LoginResponse login(LoginRequestDto loginRequestDto);

}

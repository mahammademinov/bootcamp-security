package com.company.service.mapper;

import com.company.domain.User;
import com.company.service.auth.RegisterRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User registerDtoToEntity(RegisterRequestDto registerRequestDto);

}

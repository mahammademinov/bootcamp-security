package com.company.service;

import com.company.domain.Authority;
import com.company.domain.Role;
import com.company.domain.User;
import com.company.exception.UserNotFoundException;
import com.company.repository.UserRepository;
import com.company.security.SecurityUtil;
import com.company.service.auth.LoginRequestDto;
import com.company.service.auth.LoginResponse;
import com.company.service.auth.RegisterRequestDto;
import com.company.service.mapper.UserMapper;
import java.util.Set;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final SecurityUtil securityUtil;

    private final KafkaTemplate<Object, Object> kafkaTemplate;

    @Value("${com.kafka.topic}")
    private String topic;

    @Override
    @Transactional
    public void register(RegisterRequestDto registerRequestDto) {
        var user = userMapper.registerDtoToEntity(registerRequestDto);

        Authority authority = new Authority();
        authority.setRole(Role.USER);
        user.setAuthorities(Set.of(authority));

        user.setEnabled(true);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setPassword(passwordEncoder.encode(registerRequestDto.getPassword()));

        userRepository.save(user);

        kafkaTemplate.send(topic, "userUuid", user.getUuid());
    }

    @Override
    public LoginResponse login(LoginRequestDto loginRequestDto) {
        var jwt = securityUtil.createAuthentication(loginRequestDto);

        User user = userRepository.findByUsername(loginRequestDto.getUsername())
                .orElseThrow(() -> new UserNotFoundException(new Object[]{loginRequestDto.getUsername()}));
        return LoginResponse.builder()
                .fullName(user.getFullName())
                .accessToken(jwt).build();
    }
}

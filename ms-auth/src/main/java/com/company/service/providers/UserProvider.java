package com.company.service.providers;

import com.company.domain.User;
import com.company.security.auth.service.Claim;
import com.company.security.auth.service.ClaimProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserProvider implements ClaimProvider {
    private static final String ID = "id";

    @Override
    public Claim provide(Authentication authentication) {
        log.trace("Providing {} claims", ID);
        var principal = (User) authentication.getPrincipal();
        return new Claim(ID, principal.getId().toString());
    }
}

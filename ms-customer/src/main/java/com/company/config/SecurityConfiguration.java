package com.company.config;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import com.company.security.auth.AuthenticationEntryPointConfigurer;
import com.company.security.auth.service.AuthService;
import com.company.security.auth.service.JwtService;
import com.company.security.config.BaseSecurityConfig;
import com.company.security.config.SecurityProperties;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Slf4j
@Import({
        SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class
})
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {
    private static final String CUSTOMER_API = "/api/customers";

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(POST, CUSTOMER_API + "/**").permitAll()
                .antMatchers(GET, CUSTOMER_API + "/**").authenticated();

        super.configure(http);
    }
}

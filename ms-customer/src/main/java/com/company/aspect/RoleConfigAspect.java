//package com.company.aspect;
//
//import com.company.security.auth.service.SecurityService;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//@Slf4j
//@RequiredArgsConstructor
//public class RoleConfigAspect {
//
//    private final SecurityService securityService;
//
//    @Around("@annotation(com.company.aspect.HasRole)")
//    public Object checkAuthenticationMethod(ProceedingJoinPoint joinPoint) throws Throwable {
//        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
//        HasRole annotation = signature.getMethod().getAnnotation(HasRole.class);
//
//        if (!annotation.value().equals(securityService.getCurrentUserAuthority().get())) {
//            log.warn("Exception occurs when trying to execute method");
//            throw new RuntimeException("No Permission");
//        }
//        return joinPoint.proceed();
//    }
//}

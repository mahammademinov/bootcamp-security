package com.company.aspect;

import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class SuccessRequestAspect {

    private static final Map<Object, Integer> cache = new HashMap<>();

    @AfterReturning(value = "execution(* com.company.controller.CustomerController.*(..)) " +
            "&& args(dto,..)", returning = "response")
    public void calculateSuccessRequests(JoinPoint joinPoint, Object dto, ResponseEntity<?> response) {
        if (response.getStatusCode().is2xxSuccessful()) {
            String methodName = joinPoint.getSignature().getName();
            log.info("{} method successful executed", methodName);
            cache.merge(methodName, 1, Integer::sum);
            Integer successCount = cache.get(methodName);

            log.info("{} method success count is {}", methodName, successCount);
        }
    }
}

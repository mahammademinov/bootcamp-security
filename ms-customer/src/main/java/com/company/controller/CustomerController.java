package com.company.controller;

import static org.springframework.http.HttpStatus.CREATED;

import com.company.aspect.HasRole;
import com.company.dto.CustomerResponse;
import com.company.dto.RegisterReqDto;
import com.company.security.constants.UserAuthority;
import com.company.service.CustomerService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody @Validated RegisterReqDto dto) {
        customerService.register(dto.getUserUuid());
        return ResponseEntity.status(CREATED).build();
    }

    @GetMapping("/all")
//    @HasRole(UserAuthority.USER)
    public ResponseEntity<List<CustomerResponse>> findAll() {
        return ResponseEntity.ok(customerService.findAll());
    }
}

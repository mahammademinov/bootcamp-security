package com.company.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class CustomerResponse {

    private Long id;
    private BigDecimal balance;
}

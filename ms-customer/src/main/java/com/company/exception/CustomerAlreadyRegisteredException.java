package com.company.exception;

import static com.company.exception.UniBootCampErrorCodes.CUSTOMER_ALREADY_REGISTERED;

import org.springframework.http.HttpStatus;

public class CustomerAlreadyRegisteredException extends UniBootCampGenericException {
    public CustomerAlreadyRegisteredException(Object... arguments) {
        super(409, HttpStatus.CONFLICT.name(), CUSTOMER_ALREADY_REGISTERED.code, arguments);
    }
}

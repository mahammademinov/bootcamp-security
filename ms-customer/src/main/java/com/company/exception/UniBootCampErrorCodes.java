package com.company.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UniBootCampErrorCodes {

    CUSTOMER_ALREADY_REGISTERED("CUSTOMER-ALREADY-REGISTERED");

    public final String code;
}

package com.company.service;

import com.company.dto.CustomerResponse;
import com.company.entity.Customer;
import com.company.exception.CustomerAlreadyRegisteredException;
import com.company.mappers.CustomerMapper;
import com.company.repository.CustomerRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    /*
    public void register(String userUuid) {
        var found = customerRepository.findByUserUuid(userUuid);

        if (found.isPresent()) {
            throw new CustomerAlreadyRegisteredException(userUuid, "https://google.com/");
        }

        customerRepository.save(Customer.builder().userUuid(userUuid).build());
    }
     */

    @KafkaListener(topics = "${com.kafka.topic}", groupId = "${com.kafka.group.id}")
    public void register(String userUuid) {
        log.info("Message received .. UserUuid: {}", userUuid);

        var found = customerRepository.findByUserUuid(userUuid);

        if (found.isPresent()) {
            throw new CustomerAlreadyRegisteredException(userUuid, "https://google.com/");
        }

        customerRepository.save(Customer.builder().userUuid(userUuid).build());
    }

    public List<CustomerResponse> findAll() {
        return customerRepository.findAll().stream().map(customerMapper::mapToDto).collect(Collectors.toList());
    }

}

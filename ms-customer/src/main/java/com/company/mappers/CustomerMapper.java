package com.company.mappers;

import com.company.dto.CustomerResponse;
import com.company.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerResponse mapToDto(Customer customer);
}
